package com.example.produfy

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class HabitListAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<HabitListAdapter.HabitViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var habits = emptyList<Habit>() // Cached copy of habits

    inner class HabitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textName: TextView = itemView.findViewById(R.id.textName)
        val textId: TextView = itemView.findViewById(R.id.textId)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HabitViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return HabitViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: HabitViewHolder, position: Int) {
        val current = habits[position]
        holder.textName.text = current.tag
        holder.textId.text=current.id.toString()
    }

    internal fun setHabits(habits: List<Habit>) {
        this.habits = habits
        notifyDataSetChanged()
    }

    override fun getItemCount() = habits.size
}