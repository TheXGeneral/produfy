package com.example.produfy

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.lifecycle.Observer
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.vivekkaushik.datepicker.DatePickerTimeline


class AllHabbitsActivity : AppCompatActivity() {
    private val newHabitActivityRequestCode = 1
    private lateinit var habitViewModel: HabitViewModel
    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_habits)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        getSupportActionBar()?.setTitle("All habits")


        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = HabitListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Get a new or existing ViewModel from the ViewModelProvider.
        habitViewModel = ViewModelProvider(this).get(HabitViewModel::class.java)

        // Add an observer on the LiveData returned by getAlphabetizedHabits.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        habitViewModel.allHabits.observe(this, Observer { habits ->
            // Update the cached copy of the habits in the adapter.
            habits?.let { adapter.setHabits(it) }
        })


        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(fun(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.nav_today -> {
                    startActivity(Intent(this,TodayActivity::class.java))
                    finish()
                }
                R.id.nav_statistic -> {
                    startActivity(Intent(this,StatisticActivity::class.java))
                    finish()

                }
                R.id.nav_all_habits -> {

                    startActivity(Intent(this,AllHabbitsActivity::class.java))
                    finish()
                }
                R.id.nav_notifications -> {
                    startActivity(Intent(this,NotificationsActivity::class.java))
                    finish()
                }
                R.id.nav_settings -> {
                    startActivity(Intent(this,SettingsActivity::class.java))
                    finish()
                }

            }
            drawerLayout.closeDrawer(GravityCompat.START)
            return true
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)

        if (requestCode == newHabitActivityRequestCode && resultCode == Activity.RESULT_OK) {
            intentData?.let { data ->
                var uts=System.currentTimeMillis()
                val habit = Habit(uts,"edascas","not dzxc zxdc znice","awadxcadasdfda","aesdzxcxdfvdfa","assdvc c zsdcd","dasvsdzcv zsdd","adsv sdsa","dascv szdcvsd")
                habitViewModel.insert(habit)

                Unit
            }
        } else {
            Toast.makeText(
                applicationContext,
                "Emty not saved",
                Toast.LENGTH_LONG
            ).show()
        }
    }
}
