package com.example.produfy

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView

class NotificationsActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        getSupportActionBar()?.setTitle("Notifications")

        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(fun(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.nav_today -> {
                    startActivity(Intent(this,TodayActivity::class.java))
                    finish()
                }
                R.id.nav_statistic -> {
                    startActivity(Intent(this,StatisticActivity::class.java))
                    finish()

                }
                R.id.nav_all_habits -> {

                    startActivity(Intent(this,AllHabbitsActivity::class.java))
                    finish()
                }
                R.id.nav_notifications -> {
                    startActivity(Intent(this,NotificationsActivity::class.java))
                    finish()
                }
                R.id.nav_settings -> {
                    startActivity(Intent(this,SettingsActivity::class.java))
                    finish()
                }

            }
            drawerLayout.closeDrawer(GravityCompat.START)
            return true
        })
    }
}
