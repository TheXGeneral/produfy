package com.example.produfy

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.tejpratapsingh.recyclercalendar.adapter.SimpleRecyclerCalendarAdapter
import com.tejpratapsingh.recyclercalendar.model.RecyclerCalendarConfiguration
import com.tejpratapsingh.recyclercalendar.utilities.CalendarUtils
import java.util.*


class TodayActivity : AppCompatActivity() {

    private val newHabitActivityRequestCode = 1
    private lateinit var habitViewModel: HabitViewModel
    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_today)
        // set ToolBar name
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Today"

        // handel recycler View
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = HabitListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Get a new or existing ViewModel from the ViewModelProvider.
        habitViewModel = ViewModelProvider(this).get(HabitViewModel::class.java)

        // Add an observer on the LiveData returned by getAlphabetizedHabits.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        habitViewModel.allHabits.observe(this, Observer { habits ->
            // Update the cached copy of the habits in the adapter.
            habits?.let { adapter.setHabits(it) }
        })
        /// handel floating action button
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val intent = Intent(this, NewHabitActivity::class.java)
            startActivityForResult(intent, newHabitActivityRequestCode)
        }

            //open drawer and set onclicklistener on it to redirect to all pages
        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(fun(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.nav_today -> {
                    startActivity(Intent(this,TodayActivity::class.java))
                    finish()
                }
                R.id.nav_statistic -> {
                    startActivity(Intent(this,StatisticActivity::class.java))
                    finish()

                }
                R.id.nav_all_habits -> {

                    startActivity(Intent(this,AllHabbitsActivity::class.java))
                    finish()
                }
                R.id.nav_notifications -> {
                    startActivity(Intent(this,NotificationsActivity::class.java))
                    finish()
                }
                R.id.nav_settings -> {
                    startActivity(Intent(this,SettingsActivity::class.java))
                    finish()
                }

            }
            drawerLayout.closeDrawer(GravityCompat.START)
            return true
        })
            /////////////////////////////////////////////////////////////////////////////////
        // Aded Recycle View calendar element
        val calendarRecyclerView: RecyclerView = findViewById(R.id.calendarRecyclerView)
        val textViewSelectedDate: TextView = findViewById(R.id.textViewSelectedDate)

        val date = Date()
        date.time = System.currentTimeMillis()

        val startCal = Calendar.getInstance()

        val endCal = Calendar.getInstance()
        endCal.time = date
        endCal.add(Calendar.MONTH, 3)

        val configuration: RecyclerCalendarConfiguration =
            RecyclerCalendarConfiguration(
                calenderViewType = RecyclerCalendarConfiguration.CalenderViewType.HORIZONTAL,
                calendarLocale = Locale.getDefault(),
                includeMonthHeader = true
            )

        textViewSelectedDate.text =
            CalendarUtils.dateStringFromFormat(
                locale = configuration.calendarLocale,
                date = date,
                format = CalendarUtils.LONG_DATE_FORMAT
            ) ?: ""

        val calendarAdapterHorizontal: HorizontalRecyclerCalendarAdapter =
            HorizontalRecyclerCalendarAdapter(
                startDate = startCal.time,
                endDate = endCal.time,
                configuration = configuration,
                selectedDate = date,
                dateSelectListener = object : HorizontalRecyclerCalendarAdapter.OnDateSelected {
                    override fun onDateSelected(date: Date) {
                        Toast.makeText(this@TodayActivity, CalendarUtils.dateStringFromFormat(locale = configuration.calendarLocale, date = date, format = CalendarUtils.LONG_DATE_FORMAT).toString(),Toast.LENGTH_LONG).show()
                                ?: ""
                    }
                }
            )

        calendarRecyclerView.adapter = calendarAdapterHorizontal

        val snapHelper = PagerSnapHelper() // Or LinearSnapHelper
        snapHelper.attachToRecyclerView(calendarRecyclerView)
    }
// get data from add element page and send it to database
    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)

        if (requestCode == newHabitActivityRequestCode && resultCode == Activity.RESULT_OK) {
            intentData?.let { data ->
                data.getStringExtra(NewHabitActivity.EXTRA_REPLY)?.let {


                    var uts = System.currentTimeMillis()
                    val habit = Habit(
                        uts,
                        it,
                        "edascas",
                        "awadxcadasdfda",
                        "aesdzxcxdfvdfa",
                        "assdvc c zsdcd",
                        "dasvsdzcv zsdd",
                        "adsv sdsa",
                        "dascv szdcvsd"
                    )
                    habitViewModel.insert(habit)

                }
            }
        }
    }
}
