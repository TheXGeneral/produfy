package com.example.produfy

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class NewHabitActivity : AppCompatActivity() {

    private lateinit var editHabitView: EditText

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_habit)
        editHabitView = findViewById(R.id.edit_word)

        val button = findViewById<Button>(R.id.button_save)
        button.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(editHabitView.text)) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                val habit = editHabitView.text.toString()
                replyIntent.putExtra(EXTRA_REPLY, habit)
                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }

    companion object {
        const val EXTRA_REPLY = "com.example.android.habitlistsql.REPLY"
    }
}