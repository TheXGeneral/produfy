package com.example.produfy

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private val newHabitActivityRequestCode = 1
    private lateinit var habitViewModel: HabitViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = HabitListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Get a new or existing ViewModel from the ViewModelProvider.
        habitViewModel = ViewModelProvider(this).get(HabitViewModel::class.java)

        // Add an observer on the LiveData returned by getAlphabetizedHabits.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        habitViewModel.allHabits.observe(this, Observer { habits ->
            // Update the cached copy of the habits in the adapter.
            habits?.let { adapter.setHabits(it) }
        })

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val intent = Intent(this@MainActivity, NewHabitActivity::class.java)
            startActivityForResult(intent, newHabitActivityRequestCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)

        if (requestCode == newHabitActivityRequestCode && resultCode == Activity.RESULT_OK) {
            intentData?.let { data ->
                    var uts=System.currentTimeMillis()
                val habit = Habit(uts,"edascas","not dzxc zxdc znice","awadxcadasdfda","aesdzxcxdfvdfa","assdvc c zsdcd","dasvsdzcv zsdd","adsv sdsa","dascv szdcvsd")
                habitViewModel.insert(habit)

                Unit
            }
        } else {
            Toast.makeText(
                applicationContext,
               "Emty not saved",
                Toast.LENGTH_LONG
            ).show()
        }
    }
}
