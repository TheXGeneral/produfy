package com.example.produfy

import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import java.text.SimpleDateFormat
import java.util.*


class SettingsActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        // set ToolBar name
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Settings"


        // get all elements from page

        val morningCV    = findViewById<CardView>(R.id.MorningCV)
        val afternoonCV    = findViewById<CardView>(R.id.AfternoonCV)
        val eveningCV    = findViewById<CardView>(R.id.EveningCV)
        val unitsofmeasureCV=findViewById<CardView>(R.id.UnitsofmeasureCV)
        val helpCV=findViewById<CardView>(R.id.HelpCV)
        val rateusCV=findViewById<CardView>(R.id.Rate_UsCV)
        val sendfeedbackCV=findViewById<CardView>(R.id.Send_FeedbackCV)
        val shareCV=findViewById<CardView>(R.id.ShareCV)
        val morning = findViewById<TextView>(R.id.MorningTime)
        val evening = findViewById<TextView>(R.id.EveningTime)
        val afternoon = findViewById<TextView>(R.id.AfternoonTime)
        val settings = getSharedPreferences("Time", 0)
        // get animation
        val animalpha= AnimationUtils.loadAnimation(this,R.anim.anim_alpha)

        // Fill elements with data from shared preferences
        morning.text = settings.getString("Morning", "Set time").toString()
        evening.text = settings.getString("Evening", "Set time").toString()
        afternoon.text = settings.getString("Afternoon", "Set time").toString()
            // Set on click listener on each cardview
        morningCV.setOnClickListener {
            // start animation
            it.startAnimation(animalpha)
            // create calendar and get data from it
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                // set data to TextView
               morning.text = SimpleDateFormat("HH:mm").format(cal.time)
                //set modifications to shared preferences
                 getSharedPreferences("Time", 0).edit().putString("Morning", SimpleDateFormat("HH:mm").format(cal.time).toString()).apply()


            }
            TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false).show()
        }
        afternoonCV.setOnClickListener {
            // start animation
            it.startAnimation(animalpha)
            // create calendar and get data from it
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                // set data to TextView
                afternoon.text = SimpleDateFormat("HH:mm").format(cal.time)
                //set modifications to shared preferences
                getSharedPreferences("Time", 0).edit().putString("Afternoon", SimpleDateFormat("HH:mm").format(cal.time).toString()).apply()
            }
            TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false).show()
        }
        eveningCV.setOnClickListener {
            // start animation
            it.startAnimation(animalpha)
            // create calendar and get data from it
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                // set data to TextView
                evening.text = SimpleDateFormat("HH:mm").format(cal.time)
                //set modifications to shared preferences
                getSharedPreferences("Time", 0).edit().putString("Evening", SimpleDateFormat("HH:mm").format(cal.time).toString()).apply()
            }
            TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false).show()
        }
        unitsofmeasureCV.setOnClickListener {
            // start animation
            it.startAnimation(animalpha)
            Toast.makeText(this,"Will be implemented", Toast.LENGTH_LONG).show()
        }
        helpCV.setOnClickListener {
            // start animation
            it.startAnimation(animalpha)
            Toast.makeText(this,"Will be implemented", Toast.LENGTH_LONG).show()
        }
        rateusCV.setOnClickListener {
            // start animation
            it.startAnimation(animalpha)
            Toast.makeText(this,"Will be implemented", Toast.LENGTH_LONG).show()
        }
        sendfeedbackCV.setOnClickListener {
            // start animation
            it.startAnimation(animalpha)
            Toast.makeText(this,"Will be implemented", Toast.LENGTH_LONG).show()
        }
        shareCV.setOnClickListener {
            // start animation
            it.startAnimation(animalpha)
            Toast.makeText(this,"Will be implemented", Toast.LENGTH_LONG).show()
        }

        //open drawer and set onclicklistener on it to redirect to all pages
        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(fun(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.nav_today -> {
                    startActivity(Intent(this,TodayActivity::class.java))
                    finish()
                }
                R.id.nav_statistic -> {
                    startActivity(Intent(this,StatisticActivity::class.java))
                    finish()

                }
                R.id.nav_all_habits -> {

                    startActivity(Intent(this,AllHabbitsActivity::class.java))
                    finish()
                }
                R.id.nav_notifications -> {
                    startActivity(Intent(this,NotificationsActivity::class.java))
                    finish()
                }
                R.id.nav_settings -> {
                    startActivity(Intent(this,SettingsActivity::class.java))
                    finish()
                }

            }
            drawerLayout.closeDrawer(GravityCompat.START)
            return true
        })
    }
}
