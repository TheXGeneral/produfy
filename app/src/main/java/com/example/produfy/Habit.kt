package com.example.produfy

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "habit_table")
class Habit(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "Tag") val tag: String,
    @ColumnInfo(name = "Description") val description: String,
    @ColumnInfo(name = "Sdate") val sdate: String,
    @ColumnInfo(name = "Wtd") val wtd: String,
    @ColumnInfo(name = "Color") val color: String,
    @ColumnInfo(name = "Wtr") val wtr: String,
    @ColumnInfo(name = "Icon") val icon: String,
    @ColumnInfo(name = "DaysList") val dayslist: String
)