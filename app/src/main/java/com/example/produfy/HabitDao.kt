package com.example.produfy

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface HabitDao {

    @Query("SELECT * from habit_table ORDER BY id ASC")
    fun getAlphabetizedHabits(): LiveData<List<Habit>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(word: Habit)

    @Query("DELETE FROM habit_table")
    suspend fun deleteAll()

    @Delete
    fun deleteHabit( habit:Habit)


}