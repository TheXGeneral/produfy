package com.example.produfy

import androidx.lifecycle.LiveData


// Declares the DAO as a private property in the constructor. Pass in the DAO
// instead of the whole database, because you only need access to the DAO
class HabitRepository(private val habitDao: HabitDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allHabits: LiveData<List<Habit>> = habitDao.getAlphabetizedHabits()

    suspend fun insert(habit: Habit) {
        habitDao.insert(habit)
    }
}

